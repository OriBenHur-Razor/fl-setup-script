#!/usr/bin/env bash

if [ "$EUID" -ne 0 ];then
  echo "Please run as root Or run 'sudo bash $0'"
  exit 0
fi
test_count=0
tests=()
if [[ $(nvidia-smi --query-gpu=count --format=csv,noheader) -gt 0 ]];then
  ((test_count=test_count+1))
  tests+=( "nvidia-smi" )
fi
wget https://bitbucket.org/OriBenHur-Razor/fl-setup-script/raw/a7c4b3e09c6200e7887aa8ff818600bc5ec51515/libcudnn8-samples_8.1.1.33-1%2Bcuda11.2_amd64.deb -O /tmp/libcudnn8-samples_8.1.1.33-1+cuda11.2_amd64.deb
wget https://bitbucket.org/OriBenHur-Razor/fl-setup-script/raw/c15464745da8f14fe8b52b65f897a6342cc510ac/gpu_test.py -O /tmp/gpu_test.py
sudo apt install /tmp/libcudnn8-samples_8.1.1.33-1+cuda11.2_amd64.deb -q -y >/dev/null 2>&1
sudo apt install libfreeimage3 libfreeimage-dev -y
cd /usr/src/cudnn_samples_v8/mnistCUDNN/ || exit
export PATH=/usr/local/cuda-11.0/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-11.0/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}

sudo make clean --quiet && sudo make --quiet -j "$(nproc --all)"
if [[ $(./mnistCUDNN | grep -c "Test passed!") -eq 2 ]];then
  ((test_count=test_count+1))
  tests+=( "cudnn and cuda-toolkit" )
fi
cd - || exit

sudo apt install build-essential libssl-dev libffi-dev python-dev python3-virtualenv virtualenv  -y
virtualenv test_tensorflow --python=python3.7 -q 
source test_tensorflow/bin/activate
if ! pip list | grep tensorflow-gpu  >/dev/null 2>&1;then 
  pip install tensorflow-gpu==2.4.0
  sed -i "28i np.warnings.filterwarnings('ignore')" test_tensorflow/lib/python3.7/site-packages/tensorflow/python/framework/dtypes.py
fi 
if [[ -n $(python /tmp/gpu_test.py 2> /dev/null) ]]; then
  ((test_count=test_count+1))
  tests+=( "tensorflow" )
fi

deactivate
rm -rf test_tensorflow
echo "${test_count} out of 3"
echo "${tests[@]}"