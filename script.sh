#!/usr/bin/env bash
if [ "$EUID" -ne 0 ];then 
  echo "Please run as root Or run 'sudo bash $0'"
  exit 0
fi

sudo add-apt-repository ppa:graphics-drivers/ppa -y
sudo add-apt-repository ppa:deadsnakes/ppa -y
apt-key adv --fetch-keys  http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
echo "deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64 /" | sudo tee /etc/apt/sources.list.d/cuda.list
echo "deb http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64 /" | sudo tee /etc/apt/sources.list.d/cuda_learn.list
sudo apt update
sudo apt install --no-install-recommends  curl python3.7 nvidia-driver-460 -y
sudo apt install libgl1-mesa-glx libegl1-mesa libxrandr2 libxrandr2 libxss1 libxcursor1 libxcomposite1 libasound2 libxi6 libxtst6 -y
sudo snap install pycharm-community --classic
wget https://repo.anaconda.com/archive/Anaconda3-2020.11-Linux-x86_64.sh 
bash ./Anaconda3-2020.11-Linux-x86_64.sh -b 
chown -R 1000:1000 ~

sudo apt install --no-install-recommends cuda-toolkit-11-0=11.0.3-1 libcudnn8=8.0.0.180-1+cuda11.0 libcudnn8-dev=8.0.0.180-1+cuda11.0 -y
